package Polinom;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PolinomTest {

    @Test
    void addMonom() {
        Polinom p=new Polinom(0,0);
        p.addMonom(new Monom(1,1));
        Assert.assertEquals(1,p.gradPolinom);
        Assert.assertEquals(1,p.nrTermeni);
    }

    @Test
    void deleteMonom() {
        Polinom p=new Polinom(0,0);
        p.addMonom(new Monom(1,1));
        p.deleteMonom(0);
        Assert.assertEquals(false,p.polinom.contains(0));

    }

    @Test
    void getMonom() {
        Polinom p=new Polinom(0,0);
        Monom m=new Monom(1,1);
        p.addMonom(m);
        Assert.assertEquals(m,p.getMonom(0));
    }

    @Test
    void clearPolinom() {
        Polinom p=new Polinom(0,0);
        Monom m=new Monom(1,1);
        p.addMonom(m);
        p.clearPolinom();
        Assert.assertEquals(0,p.nrTermeni);
        Assert.assertEquals(0,p.gradPolinom);

    }

    @Test
    void sortPolinom() {
        Polinom p = new Polinom(0,0);
        Monom a = new Monom(1,1);
        Monom b = new Monom(2,3);
        Monom c = new Monom(3,2);
        p.addMonom(a);
        p.addMonom(b);
        p.addMonom(c);
        p.sortPolinom();

        Assert.assertEquals(a,p.getMonom(2));
        Assert.assertEquals(b,p.getMonom(0));
        Assert.assertEquals(c,p.getMonom(1));
    }

    @Test
    void afis() {


    }

    @Test
    void noMonom() {
        Polinom p=new Polinom(0,0);
        p.addMonom(new Monom(1,1));
        p.deleteMonom(0);
        Assert.assertEquals(true,p.noMonom());

    }
}