package Polinom;


import org.junit.Assert;

import javax.management.OperationsException;

import static org.junit.jupiter.api.Assertions.*;

class OperatiiTest {

    @org.junit.jupiter.api.Test
    void suma() {
        Polinom p=new Polinom(0,0);
        p.addMonom(new Monom(1,0));
        p.addMonom(new Monom(1,1));

        Polinom q=new Polinom(0,0);
        q.addMonom(new Monom(1,1));
        q.addMonom(new Monom(1,2));

        Polinom res=Operatii.suma(p,q);
        Polinom test=new Polinom(0,0);
        test.addMonom(new Monom(1,0));
        test.addMonom(new Monom(2,1));
        test.addMonom(new Monom(1,2));

        Assert.assertEquals(res.getMonom(0).getCoeficent(),test.getMonom(0).getCoeficent(),0.0);
       Assert.assertEquals(res.getMonom(0).getExponent(),test.getMonom(2).getExponent(),0.0);

        Assert.assertEquals(res.getMonom(1).getCoeficent(),test.getMonom(1).getCoeficent(),0.0);
        Assert.assertEquals(res.getMonom(1).getExponent(),test.getMonom(1).getExponent(),0.0);

        Assert.assertEquals(res.getMonom(2).getCoeficent(),test.getMonom(2).getCoeficent(),0.0);
        Assert.assertEquals(res.getMonom(2).getExponent(),test.getMonom(0).getExponent(),0.0);

    }

    @org.junit.jupiter.api.Test
    void derivare() {
        Polinom p=new Polinom(0,0);
        p.addMonom(new Monom(2,1));
        p.addMonom(new Monom(1,2));

        Polinom res= Operatii.derivare(p);
        Polinom test =new Polinom(0,0);
        test.addMonom(new Monom(2,0));
        test.addMonom(new Monom(2,1));

        Assert.assertEquals(res.getMonom(0).getCoeficent(),test.getMonom(0).getCoeficent(),0.0);
        Assert.assertEquals(res.getMonom(0).getExponent(),test.getMonom(0).getExponent(),0.0);

        Assert.assertEquals(res.getMonom(1).getCoeficent(),test.getMonom(1).getCoeficent(),0.0);
        Assert.assertEquals(res.getMonom(1).getExponent(),test.getMonom(1).getExponent(),0.0);

    }

    @org.junit.jupiter.api.Test
    void integrare() {
        Polinom p=new Polinom(0,0);
        p.addMonom(new Monom(2,1));
        p.addMonom(new Monom(3,2));

        Polinom res= Operatii.integrare(p);
        Polinom test =new Polinom(0,0);
        test.addMonom(new Monom(1,2));
        test.addMonom(new Monom(1,3));

        Assert.assertEquals(res.getMonom(0).getCoeficent(),test.getMonom(0).getCoeficent(),0.0);
        Assert.assertEquals(res.getMonom(0).getExponent(),test.getMonom(0).getExponent(),0.0);

        Assert.assertEquals(res.getMonom(1).getCoeficent(),test.getMonom(1).getCoeficent(),0.0);
        Assert.assertEquals(res.getMonom(1).getExponent(),test.getMonom(1).getExponent(),0.0);
    }

    @org.junit.jupiter.api.Test
    void inmultire() {
        Polinom p = new Polinom(0,0);
        p.addMonom(new Monom(1,1));
        p.addMonom(new Monom(2,2));
        p.addMonom(new Monom(1,3));

        Polinom q = new Polinom(0,0);
        q.addMonom(new Monom(1,2));

        Polinom result_test = Operatii.inmultire(p,q);
        Polinom result = new Polinom(0,0);
        result.addMonom(new Monom(1,5));
        result.addMonom(new Monom(2,4));
        result.addMonom(new Monom(1,3));

        Assert.assertEquals(result.getMonom(0).getExponent(),result_test.getMonom(0).getExponent());
        Assert.assertEquals(result.getMonom(0).getCoeficent(),result_test.getMonom(0).getCoeficent(),0.0);

        Assert.assertEquals(result.getMonom(1).getExponent(),result_test.getMonom(1).getExponent());
        Assert.assertEquals(result.getMonom(1).getCoeficent(),result_test.getMonom(1).getCoeficent(),0.0);

        Assert.assertEquals(result.getMonom(2).getExponent(),result_test.getMonom(2).getExponent());
        Assert.assertEquals(result.getMonom(2).getCoeficent(),result_test.getMonom(2).getCoeficent(),0.0);

    }

    @org.junit.jupiter.api.Test
    void scadere() {
        Polinom p=new Polinom(0,0);

        p.addMonom(new Monom(1,0));
        p.addMonom(new Monom(1,1));

        Polinom q=new Polinom(0,0);

        q.addMonom(new Monom(1,1));
        q.addMonom(new Monom(1,2));

        Polinom res=Operatii.scadere(p,q);
        Polinom test=new Polinom(0,0);
        test.addMonom(new Monom(-1,2));
        test.addMonom(new Monom(1,0));
        test.addMonom(new Monom(1,0));



        Assert.assertEquals(res.getMonom(0).getCoeficent(),test.getMonom(0).getCoeficent(),0.0);
        Assert.assertEquals(res.getMonom(0).getExponent(),test.getMonom(0).getExponent(),0.0);

       Assert.assertEquals(res.getMonom(1).getCoeficent(),test.getMonom(1).getCoeficent(),0.0);
       Assert.assertEquals(res.getMonom(1).getExponent(),test.getMonom(1).getExponent(),0.0);


    }

    @org.junit.jupiter.api.Test
    void impartire() {
        Polinom p1=new Polinom(0,0);
        p1.addMonom(new Monom(1,2));
        p1.addMonom(new Monom(1,1));

        Polinom p2=new Polinom(0,0);
        p2.addMonom(new Monom(1,1));

        String res= Operatii.impartire(p1,p2);
        String test=("Q:+1.0x^1+1.0x^0;R:");
        Assert.assertEquals(res,test);


    }
}