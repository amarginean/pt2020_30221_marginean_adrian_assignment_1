package Polinom;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MonomTest {

    @Test
    void getCoeficent() {
        Monom m=new Monom(2,2);
        Assert.assertEquals(2,m.getCoeficent(),0.0);
    }

    @Test
    void setCoeficent() {
        Monom m=new Monom(2,2);
        m.setCoeficent(3);
        Assert.assertEquals(3,m.getCoeficent(),0.0);
    }

    @Test
    void getExponent() {
        Monom m=new Monom(2,2);
        Assert.assertEquals(2,m.getExponent(),0.0);
    }

    @Test
    void setExponent() {
        Monom m=new Monom(2,2);
        m.setExponent(3);
        Assert.assertEquals(3,m.getExponent(),0.0);
    }
}