package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Polinom.*;

public class Controller {
    private View view;
    private Convertor convertor;

    public Controller(View view, Convertor convertor) {
        this.view = view;
        this.convertor = convertor;

        view.addSumListener(new addSumListener());
        view.addDerivListener(new derivActionListener());
        view.addIntegrateListener(new addIntegrateListener());
        view.addInmListener(new addInmListener());
        view.addDifListener(new addDifActionListener());
        view.addDivListener(new addDivAcctionListener());

    }

    public class addSumListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                String userInput1 = view.getFirstPol();
                String userInput2 = view.getSecondPol();

                Polinom pol1, pol2;
                pol1 = Convertor.toPolynomial(userInput1);
                pol2 = Convertor.toPolynomial(userInput2);
                Polinom result = Operatii.suma(pol1, pol2);

                String final_result = Convertor.toString(result);

                view.setResult(final_result);

            } catch (NumberFormatException ex) {
                view.showError("Error Input");
            }
        }
    }

    public class derivActionListener implements ActionListener {


        public void actionPerformed(ActionEvent e) {
            try {
                String userImput1 = view.getFirstPol();
                Polinom pol1;
                pol1 = Convertor.toPolynomial(userImput1);
                Polinom result = Operatii.derivare(pol1);
                String final_result = Convertor.toString(result);

                view.setResult(final_result);

            } catch (NumberFormatException ex) {
                view.showError("Error Input");
            }
        }

    }

    public class addIntegrateListener implements ActionListener {


        public void actionPerformed(ActionEvent e) {
            try {
                String userImput1 = view.getFirstPol();
                Polinom pol1;
                pol1 = Convertor.toPolynomial(userImput1);
                Polinom result = Operatii.integrare(pol1);
                String final_result = Convertor.toString(result);

                view.setResult(final_result);

            } catch (NumberFormatException ex) {
                view.showError("Error Input");
            }
        }
    }

    public class addInmListener implements ActionListener{


        public void actionPerformed(ActionEvent e) {
            try {
                String userInput1 = view.getFirstPol();
                String userInput2 = view.getSecondPol();

                Polinom pol1, pol2;
                pol1 = Convertor.toPolynomial(userInput1);
                pol2 = Convertor.toPolynomial(userInput2);
                Polinom result = Operatii.inmultire(pol1, pol2);

                String final_result = Convertor.toString(result);

                view.setResult(final_result);

            } catch (NumberFormatException ex) {
                view.showError("Error Input");
            }
        }
    }

    public class addDifActionListener implements ActionListener{


        public void actionPerformed(ActionEvent e) {
            try {
                String userInput1 = view.getFirstPol();
                String userInput2 = view.getSecondPol();

                Polinom pol1, pol2;
                pol1 = Convertor.toPolynomial(userInput1);
                pol2 = Convertor.toPolynomial(userInput2);
                Polinom result = Operatii.scadere(pol1, pol2);

                String final_result = Convertor.toString(result);

                view.setResult(final_result);

            } catch (NumberFormatException ex) {
                view.showError("Error Input");
            }
        }
        }

        public class addDivAcctionListener implements ActionListener{


            public void actionPerformed(ActionEvent e) {
                try {
                    String userInput1 = view.getFirstPol();
                    String userInput2 = view.getSecondPol();

                    Polinom pol1, pol2;
                    pol1 = Convertor.toPolynomial(userInput1);
                    pol2 = Convertor.toPolynomial(userInput2);

                    String final_result = Operatii.impartire(pol1,pol2);

                    view.setResult(final_result);

                } catch (NumberFormatException ex) {
                    view.showError("Error Input");
                }
            }
        }
    }


