package GUI;

import javax.swing.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import Polinom.*;


public class Convertor {
    static Polinom toPolynomial(String text)
    {
        Polinom polynomial = new Polinom (0,0);
        try {
            Pattern p = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
            Matcher m = p.matcher( text );
            if(m.find()) {
                do{
                    Monom monomial = new Monom(Double.parseDouble(m.group(1)), Integer.parseInt(m.group(2)));
                    polynomial.addMonom(monomial);
                }while(m.find());
            } else throw new IllegalArgumentException("Not respecting the format");
        }
        catch(IllegalArgumentException e)
        {
            JOptionPane.showMessageDialog(null,"The input does not respect the pattern!","Error",JOptionPane.ERROR_MESSAGE);
        }
        return polynomial;
    }

    static String toString(Polinom p)
    {
        String polynomial="";
        StringBuilder sb = new StringBuilder();

        for(Monom m: p.polinom) {
            if (m.getExponent() != 0) {
                if (m.getCoeficent() > 0) {
                    if (m.getExponent() == 1 && m.getCoeficent() == 1) polynomial = sb.append("+x").toString();
                    else if (m.getCoeficent() == 1) polynomial = sb.append("+x^" + m.getExponent()).toString();
                    else if (m.getExponent() == 1) polynomial = sb.append("+" + m.getCoeficent() + "x").toString();
                    else polynomial = sb.append("+" + m.getCoeficent() + "x^" + m.getExponent()).toString();
                } else if (m.getCoeficent() == -1) polynomial = sb.append("-" + "x^" + m.getExponent()).toString();
                else polynomial = sb.append(m.getCoeficent() + "x^" + m.getExponent()).toString();
            }
            else
            if(m.getCoeficent()>0) polynomial = sb.append("+"+m.getCoeficent()).toString();
            else polynomial = sb.append(m.getCoeficent()).toString();
        }

        return polynomial;
    }
}
