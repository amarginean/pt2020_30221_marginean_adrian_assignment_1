package GUI;
import Polinom.*;
import javax.swing.*;
import javax.swing.text.LabelView;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame {

     private JTextField firstPol = new JTextField(40);
     private JTextField secondPol = new JTextField(40);
    private JTextField result = new JTextField(40);

    private JButton sumButton = new JButton("Suma");
    private JButton difButton = new JButton("Diferenta");
    private JButton inmButton = new JButton("Inmultire");
    private JButton divButton = new JButton("Divizare");
    private JButton derivButton = new JButton("Derivare");
    private JButton integerButton = new JButton("Integrare");


    public View(Convertor convertor)
    {
        JPanel panel = new JPanel();
        setTitle("Operatii Polinoame");
        setSize(800, 900);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JLabel firstPol1 = new JLabel("Insereaza primul polinom:");
        firstPol1.setFont(new Font("Tahoma", Font.BOLD, 13));
        firstPol1.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel secondPol1 = new JLabel("Insereaza al doilea polinom:");
        secondPol1.setFont(new Font("Tahoma", Font.BOLD, 13));
        secondPol1.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel result1 = new JLabel("Rezultat:");
        result1.setFont(new Font("Tahoma", Font.BOLD, 13));
        result1.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel important=new JLabel("Polinomul sa fie de forma \"(-?\\\\b\\\\d+)[xX]\\\\^(-?\\\\d+\\\\b)\"");
        important.setFont(new Font("Tahoma", Font.BOLD, 13));
        important.setHorizontalAlignment(SwingConstants.CENTER);
        important.setForeground(Color.red);
        important.setBounds(100,700,500,70);



        firstPol.setBounds(350,50,300,70);
        secondPol.setBounds(350,150,300,70);
        result.setBounds(350,250,300,70);

        firstPol1.setBounds(50,50,200,50);
        secondPol1.setBounds(50,150,200,50);
        result1.setBounds(50,250,200,50);

        sumButton.setBounds(150,400,100,50);
        difButton.setBounds(150,500,100,50);
        inmButton.setBounds(150,600,100,50);
        divButton.setBounds(400,400,100,50);
        derivButton.setBounds(400,500,100,50);
        integerButton.setBounds(400,600,100,50);

        panel.add(important);
        panel.add(firstPol);
        panel.add(secondPol);
        panel.add(result);
        panel.add(firstPol1);
        panel.add(secondPol1);
        panel.add(result1);
        panel.add(sumButton);
        panel.add(difButton);
        panel.add(inmButton);
        panel.add(divButton);
        panel.add(derivButton);
        panel.add(integerButton);

        panel.setLayout(null);
       add(panel);
       setVisible(true);


    }
    String getFirstPol(){
        return firstPol.getText();
    }
    String getSecondPol(){
        return  secondPol.getText();
    }
    void setResult(String result1){
        result.setText(result1);
    }
    void addSumListener(ActionListener sum){
        sumButton.addActionListener(sum);
    }
    void addDifListener(ActionListener dif) {
        difButton.addActionListener(dif);
    }
    void addInmListener(ActionListener inm) {
        inmButton.addActionListener(inm);
    }
    void addDivListener(ActionListener div) {
        divButton.addActionListener(div);
    }
    void addDerivListener(ActionListener der) {
        derivButton.addActionListener(der);
    }
    void addIntegrateListener(ActionListener intgr) {
        integerButton.addActionListener(intgr);
    }
    void showError(String errMessage) {
        JOptionPane.showMessageDialog(this, errMessage);
    }





}
