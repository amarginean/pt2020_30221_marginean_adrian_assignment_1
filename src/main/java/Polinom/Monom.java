package Polinom;

public class Monom {
   private double coeficent;
    private int exponent;

    public double getCoeficent() {
        return coeficent;
    }

    public void setCoeficent(double coeficent) {
        this.coeficent = coeficent;
    }

    public int getExponent() {
        return exponent;
    }

    public void setExponent(int exponent) {
        this.exponent = exponent;
    }
    public Monom(double coefficient,int exponent){
        this.setCoeficent(coefficient);
        this.setExponent(exponent);
    }
}
