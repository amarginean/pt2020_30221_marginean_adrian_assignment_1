package Polinom;

import javax.swing.*;

public class Operatii {

    private static Polinom compression(Polinom p) {
        int degree = p.gradPolinom;

        int[] v = new int[degree + 1];

        for (Monom m : p.polinom)
            v[m.getExponent()] += m.getCoeficent();

        Polinom result = new Polinom(0, degree);

        for (int i = 0; i <= degree; i++)
            if (v[i] != 0) {
                result.polinom.add(new Monom(v[i], i));
                result.nrTermeni++;
            }
        result.sortPolinom();
        if (!result.noMonom()) result.gradPolinom = result.getMonom(0).getExponent();
        else result.gradPolinom = 0;

        return result;
    }

    public static Polinom suma(Polinom p1, Polinom p2) {
        int degree;
        if (p1.gradPolinom < p2.gradPolinom) degree = p2.gradPolinom;
        else degree = p1.gradPolinom;

        Polinom result = new Polinom(0, degree);
        for (Monom m : p1.polinom) {
            result.polinom.add(m);
            result.nrTermeni++;
        }
        for (Monom m : p2.polinom) {
            result.polinom.add(m);
            result.nrTermeni++;
        }

        return compression(result);
    }

    public static Polinom derivare(Polinom p1) {
        for (Monom m : p1.polinom) {
            m.setCoeficent(m.getCoeficent() * m.getExponent());
            m.setExponent(m.getExponent() - 1);
        }
        return p1;
    }

    public static Polinom integrare(Polinom p1) {
        for (Monom m : p1.polinom) {
            m.setCoeficent(m.getCoeficent() / (m.getExponent() + 1));
            m.setExponent(m.getExponent() + 1);
        }
        return p1;
    }

    public static Polinom inmultire(Polinom p1, Polinom p2) {
        Polinom p3 = new Polinom(0, 0);
        for (Monom m : p1.polinom) {
            for (Monom m1 : p2.polinom) {
                Monom m3 = new Monom(0, 0);
                m3.setExponent(m.getExponent() + m1.getExponent());
                m3.setCoeficent(m.getCoeficent() * m1.getCoeficent());
                p3.addMonom(m3);

            }
        }
        return compression(p3);
    }

    public static Polinom scadere(Polinom p1, Polinom p2) {
        for (Monom m : p2.polinom) {
            m.setCoeficent(m.getCoeficent() * (-1));
        }
        return suma(p1, p2);
    }

    public static String impartire(Polinom p1, Polinom p2) {
        int ok=0;
        Polinom q = new Polinom(0, 0);
        Polinom r = new Polinom(0, 0);
        if (p2.getMonom(0).getCoeficent() == 0) {
            ok=1;
            JOptionPane.showMessageDialog(null, "Impartire la 0 !!!", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (p1.gradPolinom < p2.gradPolinom) {
            r = p1;
        } else
            while (p1.gradPolinom >= p2.gradPolinom) {
                Monom monom1 = new Monom(p1.getMonom(0).getCoeficent() / p2.getMonom(0).getCoeficent(), p1.gradPolinom - p2.gradPolinom);
                Polinom polinom1 = new Polinom(1, p1.gradPolinom - p2.gradPolinom);
                polinom1.polinom.add(monom1);
                q.addMonom(monom1);
                Polinom polinom2 = inmultire(polinom1, p2);
                p1 = scadere(p1, polinom2);
                polinom1.clearPolinom();
            }

            if( ok==0) {
                r = p1;
            }


        String q1, r1;
        StringBuilder sbq = new StringBuilder();
        StringBuilder sbr = new StringBuilder();
        q1 = sbq.append("Q:").toString();
        r1 = sbr.append("R:").toString();
        for (Monom m : q.polinom) {
            if (m.getCoeficent() >= 0)
                q1 = sbq.append("+" + m.getCoeficent() + "x^" + m.getExponent()).toString();
            else q1 = sbq.append(m.getCoeficent() + "x^" + m.getExponent()).toString();
        }
        for (Monom m : r.polinom) {
            if (m.getCoeficent() >= 0)
                r1 = sbr.append("+" + m.getCoeficent() + "x^" + m.getExponent()).toString();
            else r1 = sbr.append(m.getCoeficent() + "x^" + m.getExponent()).toString();
        }

        return q1 + ";" + r1;

    }
}




