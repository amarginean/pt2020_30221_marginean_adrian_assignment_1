package Polinom;

import java.util.Comparator;

public  class ComparatorGrad implements Comparator<Monom> {

        public int compare(Monom o1, Monom o2) {
            if(o1.getExponent() != o2.getExponent())
                return (o2.getExponent() - o1.getExponent());
            else return -1;
        }
    }

