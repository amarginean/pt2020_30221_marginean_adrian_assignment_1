package Polinom;

import java.util.ArrayList;
import java.util.Collections;

public class Polinom {
     public ArrayList<Monom> polinom;
    public int nrTermeni;
    public int gradPolinom;

    public Polinom(int nrTermeni,int gradPolinom){
        this.polinom=new ArrayList<Monom>();
        this.nrTermeni=nrTermeni;
        this.gradPolinom=gradPolinom;
    }

    public void addMonom(Monom monom){
        polinom.add(monom);
        if(monom.getExponent()>gradPolinom)
            this.gradPolinom=monom.getExponent();
        nrTermeni++;
    }

    public void deleteMonom(int i){
        polinom.remove(i);
        nrTermeni--;
    }

    public Monom getMonom(int i){
        return polinom.get(i);
    }


    public void clearPolinom(){
        polinom.clear();
        nrTermeni=0;
        gradPolinom=0;
    }

    public void sortPolinom(){
        ComparatorGrad comparatorGrad=new ComparatorGrad();
        Collections.sort(polinom,comparatorGrad);

    }
    public void afis(Polinom polinom){
        for(Monom m:polinom.polinom){
            System.out.println(m.getCoeficent() + " X "+m.getExponent());
        }

    }

    public  boolean noMonom(){
        return polinom.isEmpty();
    }
}
